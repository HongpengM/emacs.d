(require 'use-package)

(use-package haskell-snippets
  :requires yasnippet)
(use-package haskell-mode
  :ensure t
  :mode "\\.hs\\'"
  ;;  (add-to-list 'auto-mode-alist )
  )
(use-package intero
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'intero-mode))

(provide 'init-haskell)
