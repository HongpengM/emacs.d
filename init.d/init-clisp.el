(require 'use-package)

; quicklisp-slime-helper
(load (expand-file-name "~/quicklisp/slime-helper.el"))
  ;; Replace "sbcl" with the path to your implementation
  (setq inferior-lisp-program "sbcl")

(use-package slime
  :config
  
  (setq inferior-lisp-program (shell-command-to-string "which sbcl | xargs echo -n"))
  (setq slime-contribs '(slime-fancy)))

(use-package slime-company
  :ensure t
  :requires slime
  :config
  (slime-setup '(slime-fancy slime-company))
  (add-hook 'slime-connected-hook #'slime-company-maybe-enable))

(provide 'init-clisp)
