(require 'use-package)

(use-package 
  geiser 
  :ensure t 
  :config (progn
	    ;; Boost with geiser in order to avoid org-wiki-export-html problems
	    (setq geiser-active-implementations '(mit))
	    ;;(load "~/.emacs.d/scheme/xscheme.el")
	    (load "~/.emacs.d/init.d/ob-scheme.el") 
	    (add-to-list 'exec-path "/usr/local/bin")))




(provide 'init-scheme)
