(require 'use-package)

(use-package ox-reveal
  :requires org
  :config (setq org-reveal-root "/Users/k/Apps/reveal.js")
  (setq org-reveal-hlevel 2)
  )



(provide 'init-presentation)
