(require 'use-package)

(use-package edit-server
  :config
  (progn
    (edit-server-start)))

(provide 'init-editserver)
